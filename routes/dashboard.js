var express = require('express');
const router = express.Router();
const auth = require('../authentication/auth')();
const knex = require('knex');
const dbConfig = require('../knexfile').development;
const path = require('path');
var t = require('tcomb-validation');
var validations = require('./validations');
var inspect = require('eyes').inspector({ maxLength: 20000 });
const PDFMerge = require('pdf-merge');
var merge = require('easy-pdf-merge');

router.get('/rfnorange', async (req, res) => {

  let db
  try{
 db = knex(dbConfig);

  let rfnoRange = await db
    .select('rfNo')
    .from('FanList')
    .orderBy('rfNo', 'asc');

    await db.destroy();
  if (rfnoRange) {
    return res.status(200).json([
      {
        startingRfNo: rfnoRange[0].rfNo,
        endingRfNo: rfnoRange[rfnoRange.length - 1].rfNo
      }
    ]);
  } else {
    return res.status(404).json('not found');
  }
}
catch(error){
  await db.destroy();
  return res.status(500).json('something wrong');
}
});
formatNumber = number =>
  number.toLocaleString('en-IN', {
    maximumFractionDigits: 2,
    style: 'currency',
    currency: 'INR'
  });

// router.get('/totalRfno', async (req, res) => {
//   const db = knex(dbConfig);
//   const Count = await db.from('FanList').count('id');

//   const Data = await db
//     .select('rfNo', 'id')
//     .from('FanList')
//     .orderBy('rfNo');

//   let start = 0;
//   let end = start + 100;
//   let result = [];
//   for (let i = 0; i <= 29909 / 100; ++i) {
//     let range = Data.filter(x => start < x.rfNo && end >= x.rfNo).map(
//       x => x.rfNo
//     );
//     result.push(
//       parseInt(start) + ',' + parseInt(end) + ',' + parseInt(range.length)
//     );
//     start = start + 100;
//     end = 100 + end;
//   }
//   if (true) {
//     res.status(200).json({ result });
//   } else {
//     res.status(404).json('User not Existed');
//   }
// });

router.get('/data', async (req, res) => {
      let db;
  try {
    db = knex(dbConfig);
    const d = new Date();
    const currentYear = parseInt(d.getFullYear());

    let totalFans = await db
      .select('*')
      .from('FanList')
      .where('yearOfMfg', req.query.yearOfMfg);
  
    let Price = await db
      .sum(`price as totalPrice`)
      .from('FanList')
      .where('yearOfMfg', req.query.yearOfMfg);
    let highestRevenue = await db
      .from('FanList')
      .max('price as highestRevenue')
      .where('yearOfMfg', req.query.yearOfMfg);

      await db.destroy();
    if (totalFans) {
      return res.status(200).json({
        totalFans: totalFans.length,
        totalPrice: formatNumber(Math.round((Price[0].totalPrice/100000) * 100) / 100+' Lakhs'),
        highestRevenue: formatNumber(Math.round((highestRevenue[0].highestRevenue/100000) * 100) / 100+' Lakhs')
      });
    } else {
      return res.status(404).json('not found');
    }
  } catch (error) {
    await db.destroy();
    return res.status(500).json('somthing wrong');
  }
});

router.get('/price/byYear', async (req, res) => {

  let db 
  try{
    db = knex(dbConfig);
  let pricebyYear = await db
    .select(db.raw(`sum(price)`), 'yearOfMfg')
    .from('FanList')
    .groupBy('yearOfMfg')
    .orderBy('yearOfMfg', 'asc');
  
    await db.destroy();
  if (pricebyYear) {
    return res
      .status(200)
      .json(pricebyYear.map(x => ({ sum: x.sum, yearOfMfg: x.yearOfMfg })).filter(x=>x.sum!==0));
  } else {
    return res.status(404).json('not found');
  }
}
catch (error) {
  await db.destroy();
  return res.status(500).json('somthing wrong');
}
});
router.get('/customerlist', async (req, res) => {
  let db;
  try{ 
    db = knex(dbConfig);
  const d = new Date();
  const currentYear = parseInt(d.getFullYear());
  let customerList = await db
    .select('price', 'customerName')
    .where('yearOfMfg', req.query.yearOfMfg)
    .from('FanList')
    .orderBy('price', 'desc')
    .limit(5);
    await db.destroy();
  if (customerList) {
    return res.status(200).json(
      customerList.map(x => ({
        price: formatNumber(Math.round(x.price/100000 * 100) / 100+' Lakhs'),
        customerName: x.customerName
      }))
    );
  } else {
    return res.status(404).json('not found');
  }
}
catch (error) {
  await db.destroy();
  return res.status(500).json('somthing wrong');
}
});

router.get('/fantypelist', async (req, res) => {
  let db 
  try{
  db = knex(dbConfig);
  const d = new Date();

  const currentYear = parseInt(d.getFullYear());

  let fanModelList = await db
    .select(
      db.raw(`count(id)`),
      db.raw(`split_part(\"FanList\".\"fanModel\",' ',1) as "fanType"`)
    )
    .from('FanList')
    .where('yearOfMfg', req.query.yearOfMfg)
    .groupBy('fanType');

    await db.destroy();
  if (fanModelList) {
    return res
      .status(200)
      .json(
        fanModelList
          .filter(x => x.fanType.length < 4)
          .map(x => ({ count: parseInt(x.count), fanType: x.fanType }))
      );
  } else {
    return res.status(404).json('not found');
  }
}
catch (error) {
  await db.destroy();
  return res.status(500).json('somthing wrong');
}
});

router.get('/projectEmpty', async (req, res) => {
  let db 
  try{
  db = knex(dbConfig);
  let projectnameEmpty = await db
    .select(`${req.query.field}`, 'rfNo')
    .from('FanList')
    .where(`${req.query.field}`, '')
    .orderBy('rfNo', 'asc');

    await db.destroy();
  if (projectnameEmpty) {
    return res.status(200).json(projectnameEmpty.map(x => x.rfNo));
  } else {
    return res.status(404).json('not found');
  }
}
catch (error) {
  await db.destroy();
  return res.status(500).json('somthing wrong');
}
});

router.get('/projectnull', async (req, res) => {
  let db 
  try{
  db = knex(dbConfig);
  let projectnameEmpty = await db
    .select(`${req.query.field}`, 'rfNo')
    .from('FanList')
    .where(`${req.query.field}`, 0)
    .orderBy('rfNo', 'asc');

    await  db.destroy();
  if (projectnameEmpty) {
    return res.status(200).json(projectnameEmpty.map(x => x.rfNo));
  } else {
    return res.status(404).json('not found');
  }
}
catch (error) {
  await  db.destroy();
  return res.status(500).json('somthing wrong');
}
});
module.exports = router;
