var express = require('express');
const router = express.Router();
const auth = require('../authentication/auth')();
const knex = require('knex');
const dbConfig = require('../knexfile').development;
const xlsxtojson = require('xlsx-to-json-lc');
const xlstojson = require('xls-to-json-lc');
const multer = require('multer');
const fs = require('fs');
const path = require('path');
var t = require('tcomb-validation');
var validations = require('./validations');
var PdfReader = require('pdfreader').PdfReader;
var PDFParser = require('pdf2json');
var pdfUtil = require('pdf-to-text');
var imageToTextDecoder = require('image-to-text');
var pdf_extract = require('pdf-extract');
var inspect = require('eyes').inspector({ maxLength: 20000 });
const PDFMerge = require('pdf-merge');
var merge = require('easy-pdf-merge');

const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    console.log('storage');
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    console.log('storage');

    cb(
      null,
      file.fieldname +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});

const upload = multer({
  //multer settings
  storage: storage,
  limits: {
    fileSize: 52428800
  },
  fileFilter: function(req, file, callback) {
    //file filter
    console.log('abcdefgh');
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

router.post('/excelupload', async function(req, res) {
  req.setTimeout(0);
  var exceltojson;
  upload(req, res, function(err) {
    if (err) {
      res.status(400).json({ error_code: 1, err_desc: err });
      return;
    }

    if (!req.file) {
      res.status(400).json({ error_code: 1, err_desc: 'No file passed' });
      return;
    }

    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }

    try {
      exceltojson(
        {
          input: req.file.path, //the same path where we uploaded our file
          output: null, //since we don't need output.json
          lowerCaseHeaders: false
        },
        async function(err, result) {
          try {
            if (err) {
              return res.json({ error_code: 1, err_desc: err, data: null });
            }

            const result1 = result
              .filter(o => o['RF No.'] != null && o['RF No.'] != '')

              .map(o => ({
                rfNo: o['RF No.'],
                customerName: o['Customer Name'],
                projectName: o['Project Name'],
                application: o['Application'],
                fanModel: o['Fan Model'],
                flow: isNaN(parseFloat(o['Flow       m3 / hr']))
                  ? 0
                  : o['Flow       m3 / hr'],
                staticPressure: isNaN(parseFloat(o['Static Pressure mm WG']))
                  ? 0
                  : o['Static Pressure mm WG'],
                temperature: isNaN(parseFloat(o['Temp 0C'])) ? 0 : o['Temp 0C'],
                density: isNaN(parseFloat(o['Density  Kg/ m3']))
                  ? 0
                  : o['Density  Kg/ m3'],
                shaftPower: isNaN(parseFloat(o['Shaft     Power        Kw']))
                  ? 0
                  : o['Shaft     Power        Kw'],
                fanSpeed: isNaN(parseFloat(o['Fan     Speed  RPM']))
                  ? 0
                  : o['Fan     Speed  RPM'],
                motorPower: isNaN(parseFloat(o['Motor Power    Kw']))
                  ? 0
                  : o['Motor Power    Kw'],
                motorSpeed: isNaN(parseFloat(o['Motor     Speed       RPM']))
                  ? 0
                  : o['Motor     Speed       RPM'],
                yearOfMfg: isNaN(
                  parseFloat(o['Year                of                Mfg.'])
                )
                  ? 0
                  : o['Year                of                Mfg.'],
                price: o['Price / Fan'],
                isGaDrawing: o['GA Drawing'] === 'Yes' ? true : false,
                isFanCurves: o['Fan Curves'] === 'Yes' ? true : false,
                isActive: true,
                created_at: new Date().toISOString(),
                updated_at: new Date().toISOString()
              }));
            const allRecords = [];

            const values = result1.map(x => {
              if (x.rfNo.includes('-') && !x.rfNo.includes(',')) {
                const split = x.rfNo.split('-');
                const split1 = parseInt(split[0].trim());
                const split2 = parseInt(split[1].trim());

                for (var i = split1; i <= split2; i++) {
                  allRecords.push({ ...x, rfNo: parseInt(i) });
                }
              } else if (x.rfNo.includes(',')) {
                const split = x.rfNo.split(',');
                const newRecords = [];
                const records = split.map(x => {
                  if (x.includes('-')) {
                    const split = x.split('-');
                    const split1 = parseInt(split[0].trim());
                    const split2 = parseInt(split[1].trim());

                    for (var i = split1; i <= split2; ++i) {
                      newRecords.push(parseInt(i));
                    }
                  } else {
                    newRecords.push(x);
                  }
                });
                for (var i = 0; i < newRecords.length; ++i) {
                  allRecords.push({ ...x, rfNo: parseInt(newRecords[i]) });
                }
              } else {
                allRecords.push(x);
              }
            });
            const db = knex(dbConfig);
            const allRFNOs = await db.select('rfNo').from('FanList');
            const olddata = allRFNOs.map(x => x.rfNo);

            const postData = allRecords
              .filter(x => parseInt(x.rfNo) >= 500)
              .map(x => ({
                ...x,
                price: x.price.includes('$')
                  ? parseFloat(
                      x.price
                        .split('$')[1]
                        .replace(/[^0-9 ]/g, '')
                        .trim()
                    ) * 71
                  : x.price.includes('€')
                  ? parseFloat(
                      x.price
                        .split('€')[1]
                        .replace(/[^0-9 ]/g, '')
                        .trim()
                    ) * 81
                  : x.price === ''
                  ? 0
                  : parseInt(x.price.replace(/[^0-9 ]/g, '').trim())
              }));

            const postDataAllRfNo = postData.map(x => parseInt(x.rfNo));
            const existedrfNos = olddata.filter(x =>
              postDataAllRfNo.includes(parseInt(x))
            );
            console.log(olddata.length);
            console.log(postDataAllRfNo.length);
            console.log(existedrfNos.length);
            const deleteExistedRfNos = await db
              .from('FanList')
              .whereIn('rfNo', existedrfNos)
              .del();
            console.log(deleteExistedRfNos);
            const data = await db.transaction(async function(tr) {
              return await db.batchInsert('FanList', postData).transacting(tr);
            });
            if (data) {
              return res.status(200).json({ message: 'success' });
            }
          } catch (error) {
            return res.status(500).json({ message: error.message });
          }
        }
      );
    } catch (e) {
      res.json({ error_code: 1, err_desc: 'Corrupted excel file' });
    }
  });
});

//To get Ga Drawing files from directory these are the required file formats

const getGaDrawings = fileName => {
  if (
    fileName.includes('_') &&
    !fileName.includes('TO') &&
    !fileName.includes('To') &&
    !fileName.includes('to') &&
    !fileName.includes('&')
  ) {
    const values = fileName.split('_');
    const start = values[0];
    const end =
      values[0].trim().length === values[1].trim().length
        ? values[1]
        : values[0];
    return { fileName: fileName, start: start, end: end };
  }

  if (
    fileName.includes('TO') &&
    !fileName.includes('To') &&
    !fileName.includes('to') &&
    !fileName.includes('&')
  ) {
    const values = fileName.split('TO');
    const start = values[0].trim();
    const z = values[1].split('_')[0].trim();
    const end =
      start.length > z.length
        ? start.substring(0, start.length - z.length) + z
        : z;
    return { fileName: fileName, start: start, end: end };
  }

  if (
    !fileName.includes('TO') &&
    fileName.includes('To') &&
    !fileName.includes('to') &&
    !fileName.includes('&')
  ) {
    const values = fileName.split('To');
    const start = values[0].trim();
    const z = values[1].split('_')[0].trim();
    const end =
      start.length > z.length
        ? start.substring(0, start.length - z.length) + z
        : z;
    return { fileName: fileName, start: start, end: end };
  }

  if (
    !fileName.includes('TO') &&
    !fileName.includes('To') &&
    fileName.includes('to') &&
    !fileName.includes('&')
  ) {
    const values = fileName.split('to');
    const start = values[0].trim();
    const z = values[1].split('_')[0].trim();
    const end =
      start.length > z.length
        ? start.substring(0, start.length - z.length) + z
        : z;
    return { fileName: fileName, start: start, end: end };
  }
  if (fileName.includes('&')) {
    const values = fileName.split('&');
    const start = values[0].split('RF')[1];
    const end = values[1].split('_')[0];
    return { fileName: fileName, start: start, end: end };
  }
};

router.get('/gaDrawnings', (req, res) => {
  Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
      if (!arr.includes(this[i])) {
        arr.push(this[i]);
      }
    }
    return arr;
  };

  fs.readdir('./GA Drawings', (err, files) => {
    if (!err) {
      const sortFiles = files
        .sort()
        .filter(x => getGaDrawings(x) !== undefined)
        .map(x => getGaDrawings(x));

      const rangeFiles = sortFiles
        .filter(
          x => /[a-z]/.test(x.start) === false && /[a-z]/.test(x.end) === false
        )
        .map(x => ({
          ...x,
          ...{
            start: x.start.trim(),
            end:
              x.start.trim().length > x.end.trim().length
                ? x.start
                    .trim()
                    .substring(0, x.start.trim().length - x.end.trim().length) +
                  x.end.trim()
                : x.end.trim()
          }
        }))
        .filter(
          x =>
            parseInt(req.query.rfNo) >= x.start &&
            parseInt(req.query.rfNo) <= x.end
        );

      if (rangeFiles.length === 0)
        return res.status(404).json({
          message: 'file not found'
        });
      return res.json({ data: rangeFiles.unique() });
    }
  });
});

const getRangeCurve = fileName => {
  if (fileName.includes('&')) {
    const values = fileName.split('&');
    const start = values[0].split('RF')[1];
    const end = values[1].includes('_')
      ? values[1].split('_')[0]
      : values[1].split(' ')[0] === ''
      ? values[1].split(' ')[1]
      : values[1].split(' ')[0];
    return { fileName: fileName, start: start, end: end };
  }

  if (
    !fileName.includes('to ') &&
    !fileName.includes('&') &&
    fileName.includes('RF')
  ) {
    const values = fileName.split('RF')[1].trim();
    const start = values.includes('_')
      ? values.split('_')[0]
      : values.split(' ')[0] === ''
      ? values.split(' ')[1]
      : values.split(' ')[0];
    const end = start.trim().includes('.')
      ? start.split('.')[0]
      : start.split(' ')[0];
    const end1 = end.includes('-') ? end.split('-')[1] : end;
    return { fileName: fileName, start: end1, end: end1 };
  }
  if (
    !fileName.includes('&') &&
    !fileName.includes('TO') &&
    !fileName.includes('To') &&
    fileName.includes('to ')
  ) {
    const values = fileName.split('to');
    const start = values[0].split('RF')[1];
    const end = values[1].includes('_')
      ? values[1].split('_')[0]
      : values[1].split(' ')[0] === ''
      ? values[1].split(' ')[1]
      : values[1].split(' ')[0];
    return { fileName: fileName, start: start, end: end };
  }
};

router.get('/curves', (req, res) => {
  Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
      if (!arr.includes(this[i])) {
        arr.push(this[i]);
      }
    }
    return arr;
  };

  fs.readdir('./Curves/', (err, files) => {
    if (!err) {
      const sortFiles = files
        .sort()
        .filter(x => getRangeCurve(x) !== undefined)
        .map(x => getRangeCurve(x));

      const rangeFiles = sortFiles
        .filter(
          x => /[a-z]/.test(x.start) === false && /[a-z]/.test(x.end) === false
        )
        .map(x => ({
          ...x,
          ...{
            start: x.start.trim(),
            end:
              x.start.trim().length > x.end.trim().length
                ? x.start
                    .trim()
                    .substring(0, x.start.trim().length - x.end.trim().length) +
                  x.end.trim()
                : x.end.trim()
          }
        }))
        .filter(
          x =>
            parseInt(req.query.rfNo) >= x.start &&
            parseInt(req.query.rfNo) <= x.end
        )
        .map(x => x.fileName)
        .unique();
      if (rangeFiles.length === 0)
        return res.status(404).json({
          message: 'file not found'
        });
      return res.json(rangeFiles);
    }
  });
});

const getRangePo = fileName => {
  if (
    !fileName.includes('RF') &&
    !fileName.includes('TO') &&
    !fileName.includes('To') &&
    !fileName.includes('to')
  ) {
    const values = fileName.split('.');
    const start = values[0];
    const end = values[0];

    return { fileName: fileName, start: start, end: end };
  }

  if (
    fileName.includes('RF') &&
    !fileName.includes('TO') &&
    !fileName.includes('To') &&
    !fileName.includes('to') &&
    !fileName.includes('&')
  ) {
    const values = fileName.split('RF');
    const start =
      values[1].split(' ')[0] === ''
        ? values[1].split(' ')[1]
        : values[1].split(' ')[0];
    const end = start;

    return { fileName: fileName, start: start, end: end };
  }
  if (
    fileName.includes('-') &&
    fileName.includes('_') &&
    !fileName.includes('TO') &&
    !fileName.includes('To') &&
    !fileName.includes('to')
  ) {
    const values = fileName.split('-');
    const start =
      values[0].split('RF')[1] === ''
        ? values[0].split('RF')[2]
        : values[0].split('RF')[1];
    const end =
      values[1].split(' ')[0] === ''
        ? values[1].split(' ')[1]
        : values[1].split(' ')[0];

    return { fileName: fileName, start: start, end: end };
  }

  if (
    !fileName.includes('TO') &&
    !fileName.includes('To') &&
    fileName.includes('to') &&
    fileName.includes('RF')
  ) {
    const values = fileName.split('RF');
    const start = values[1].split('to')[0];
    const tovalue = values[1].split('to')[1];
    const end =
      tovalue === undefined
        ? start
        : tovalue.includes('_')
        ? tovalue.split('_')[0]
        : tovalue.split(' ')[0] === ''
        ? tovalue.split(' ')[1]
        : tovalue.split(' ')[0];

    return { fileName: fileName, start: start, end: end };
  }

  if (fileName.includes('&')) {
    const values = fileName.split('&');
    const start = values[0].split('RF')[1];
    const end = values[1].split(' ')[1];
    return { fileName: fileName, start: start, end: end };
  }
};
router.get('/po', (req, res) => {
  Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
      if (!arr.includes(this[i])) {
        arr.push(this[i]);
      }
    }
    return arr;
  };
  fs.readdir('./PO/', (err, files) => {
    if (!err) {
      const sortFiles = files
        .sort()
        .filter(x => getRangePo(x) !== undefined)
        .map(x => getRangePo(x));

      const rangePoFiles = sortFiles
        .filter(
          x => /[a-z]/.test(x.start) === false && /[a-z]/.test(x.end) === false
        )
        .map(x => ({
          ...x,
          ...{
            start: x.start.trim(),
            end:
              x.start.trim().length > x.end.trim().length
                ? x.start
                    .trim()
                    .substring(0, x.start.trim().length - x.end.trim().length) +
                  x.end.trim()
                : x.end.trim()
          }
        }))
        .filter(
          x =>
            parseInt(req.query.rfNo) >= x.start &&
            parseInt(req.query.rfNo) <= x.end
        )
        .map(x => x.fileName)
        .unique();
      if (rangePoFiles.length === 0)
        return res.status(404).json({
          message: 'file not found'
        });
      return res.json(rangePoFiles);
    }
  });
});

// TO GET ALL 3 TYPES OF FILES RELATED TO RF NUMBER
router.get('/mergefile', (req, res) => {
  let deleteFile = false;
  Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
      if (!arr.includes(this[i])) {
        arr.push(this[i]);
      }
    }
    return arr;
  };
  fs.readdir('./Curves/', (err, files) => {
    if (!err) {
      const sortFiles = files
        .sort()
        .filter(x => getRangeCurve(x) !== undefined)
        .map(x => getRangeCurve(x));
      console.log('curves');
      const rangeCurvedFiles = sortFiles
        .filter(
          x => /[a-z]/.test(x.start) === false && /[a-z]/.test(x.end) === false
        )
        .map(x => ({
          ...x,
          ...{
            start: x.start.trim(),
            end:
              x.start.trim().length > x.end.trim().length
                ? x.start
                    .trim()
                    .substring(0, x.start.trim().length - x.end.trim().length) +
                  x.end.trim()
                : x.end.trim()
          }
        }))
        .filter(
          x =>
            parseInt(req.query.rfNo) >= x.start &&
            parseInt(req.query.rfNo) <= x.end
        );
      const multileCurveFiles = rangeCurvedFiles
        .map(x => path.join('./Curves', x.fileName))
        .unique();

      fs.readdir('./GA Drawings/', (err, files) => {
        if (!err) {
          const sortFiles = files
            .sort()
            .filter(x => getGaDrawings(x) !== undefined)
            .map(x => getGaDrawings(x));
          console.log('ga');
          const rangeGaDrawningFiles = sortFiles.filter(
            x =>
              parseInt(req.query.rfNo) >= x.start &&
              parseInt(req.query.rfNo) <= x.end
          );
          const multileGaFiles = rangeGaDrawningFiles
            .map(x => path.join('./GA Drawings', x.fileName))
            .unique();

          fs.readdir('./PO/', (err, files) => {
            if (!err) {
              const sortFiles = files
                .sort()
                .filter(x => getRangePo(x) !== undefined)
                .map(x => getRangePo(x));
              console.log('po');
              const rangePoFiles = sortFiles
                .filter(
                  x =>
                    /[a-z]/.test(x.start) === false &&
                    /[a-z]/.test(x.end) === false
                )
                .map(x => ({
                  ...x,
                  ...{
                    start: x.start.trim(),
                    end:
                      x.start.trim().length > x.end.trim().length
                        ? x.start
                            .trim()
                            .substring(
                              0,
                              x.start.trim().length - x.end.trim().length
                            ) + x.end.trim()
                        : x.end.trim()
                  }
                }));

              const multilePOFiles = rangePoFiles
                .filter(
                  x =>
                    parseInt(req.query.rfNo) >= x.start &&
                    parseInt(req.query.rfNo) <= x.end
                )
                .map(x => path.join('./PO', x.fileName))
                .unique();
              const newPdf = multileCurveFiles
                .concat(multileGaFiles)
                .concat(multilePOFiles);

              if (newPdf.length === 1) {
                return res
                  .status(200)
                  .json({ message: 'single file cannot be merge' });
              }
              if (newPdf.length > 1) {
                fs.readdir('merge', (err, files) => {
                  if (err) throw err;

                  for (const file of files) {
                    fs.unlink(path.join('merge', file), err => {
                      if (err) throw err;
                    });
                  }
                });

                merge(newPdf, `./merge/${req.query.rfNo}.pdf`, function(err) {
                  if (err) {
                    return err;
                  }
                  if (req.query.type === undefined) {
                    return res.json(newPdf);
                  }
                  if (req.query.type === 'view') {
                    fs.readFile(`./merge/${req.query.rfNo}.pdf`, function(
                      err,
                      data
                    ) {
                      res.contentType('application/pdf');
                      res.send(data);
                    });
                  }
                  if (req.query.type === 'download') {
                    res.download(
                      `./merge/${req.query.rfNo}.pdf`,
                      `/${req.query.rfNo}.pdf`
                    );
                  }
                });
              } else {
                return res.status(200).json('no data found');
              }
            }
          });
        }
      });
    }
  });
});

const reqParamsValidation = (req, res, next) => {
  let result = t.validate(req.params, validations.downloadValid);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};

// To download files of Curves , GA drawning, PO files
router.get('/:type', reqParamsValidation, (req, res) => {
  Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
      if (!arr.includes(this[i])) {
        arr.push(this[i]);
      }
    }
    return arr;
  };
  if (req.params.type === 'poDownload') {
    let poFiles = [];
    fs.readdir('./PO/', (err, files) => {
      if (!err) {
        const sortFiles = files
          .sort()
          .filter(x => getRangePo(x) !== undefined)
          .map(x => getRangePo(x));

        const rangeFiles = sortFiles
          .filter(
            x =>
              /[a-z]/.test(x.start) === false && /[a-z]/.test(x.end) === false
          )
          .map(x => ({
            ...x,
            ...{
              start: x.start.trim(),
              end:
                x.start.trim().length > x.end.trim().length
                  ? x.start
                      .trim()
                      .substring(
                        0,
                        x.start.trim().length - x.end.trim().length
                      ) + x.end.trim()
                  : x.end.trim()
            }
          }))
          .filter(
            x =>
              parseInt(req.query.rfNo) >= x.start &&
              parseInt(req.query.rfNo) <= x.end
          )
          .map(x => x.fileName)
          .unique();

        if (!req.query.fileIndex) {
          if (!rangeFiles)
            return res.status(404).json({
              message: 'file not found'
            });
          return res.json(rangeFiles);
        }
        if (req.query.fileIndex) {
          if (req.query.fileIndex < rangeFiles.length) {
            const fileName = rangeFiles[req.query.fileIndex];
            var filepath = path.join('./PO', fileName);
            if (!req.query.type) {
              fs.readFile(filepath, function(err, data) {
                res.contentType('application/pdf');
                res.send(data);
              });
            }
            if (req.query.type === 'download') {
              res.download(filepath, fileName);
            }
          } else {
            return res.status(404).json({
              message: 'file not found'
            });
          }
        }
      }
    });
  } else if (req.params.type === 'gadrawningDownload') {
    fs.readdir('./GA Drawings/', (err, files) => {
      if (!err) {
        const sortFiles = files
          .sort()
          .filter(x => getGaDrawings(x) !== undefined)
          .map(x => getGaDrawings(x));

        const rangeFiles = sortFiles
          .filter(
            x =>
              parseInt(req.query.rfNo) >= x.start &&
              parseInt(req.query.rfNo) <= x.end
          )
          .map(x => x.fileName)
          .unique();

        if (!req.query.fileIndex) {
          if (!rangeFiles)
            return res.status(404).json({
              message: 'file not found'
            });
          return res.json(rangeFiles);
        }
        if (req.query.fileIndex) {
          if (req.query.fileIndex < rangeFiles.length) {
            const fileName = rangeFiles[req.query.fileIndex];
            var filepath = path.join('./GA Drawings', fileName);
            if (!req.query.type) {
              fs.readFile(filepath, function(err, data) {
                res.contentType('application/pdf');
                res.send(data);
              });
            }
            if (req.query.type === 'download') {
              res.download(filepath, fileName);
            }
          } else {
            return res.status(404).json({
              message: 'file not found'
            });
          }
        }
      }
    });
  } else if (req.params.type === 'curvesDownload') {
    fs.readdir('./Curves/', (err, files) => {
      if (!err) {
        const sortFiles = files
          .sort()
          .filter(x => getRangeCurve(x) !== undefined)
          .map(x => getRangeCurve(x));

        const rangeFiles = sortFiles
          .filter(
            x =>
              /[a-z]/.test(x.start) === false && /[a-z]/.test(x.end) === false
          )
          .map(x => ({
            ...x,
            ...{
              start: x.start.trim(),
              end:
                x.start.trim().length > x.end.trim().length
                  ? x.start
                      .trim()
                      .substring(
                        0,
                        x.start.trim().length - x.end.trim().length
                      ) + x.end.trim()
                  : x.end.trim()
            }
          }))
          .filter(
            x =>
              parseInt(req.query.rfNo) >= x.start &&
              parseInt(req.query.rfNo) <= x.end
          )
          .map(x => x.fileName)
          .unique();
        if (!req.query.fileIndex) {
          if (!rangeFiles)
            return res.status(404).json({
              message: 'file not found'
            });
          return res.json(rangeFiles);
        }
        if (req.query.fileIndex) {
          if (req.query.fileIndex < rangeFiles.length) {
            const fileName = rangeFiles[req.query.fileIndex];
            var filepath = path.join('./Curves', fileName);
            if (!req.query.type) {
              fs.readFile(filepath, function(err, data) {
                res.contentType('application/pdf');
                res.send(data);
              });
            }
            if (req.query.type === 'download') {
              res.download(filepath, fileName);
            }
          } else {
            return res.status(404).json({
              message: 'file not found'
            });
          }
        }
      }
    });
  }
});

const reqQueryValidation = (req, res, next) => {
  let result = t.validate(req.query, validations.pagination);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};

// PAGINATION
router.get('/', (req, res) => {
  const db = knex(dbConfig);

  const query = db('FanList').select(
    '*',
    db.raw(`split_part(\"FanList\".\"fanModel\",' ',1) as "fanType"`)
  );

  const Count = db('FanList').count('id');

  if (req.query.rfNo && req.query.rfNo !== '') {
    if (req.query.rfNo.includes('-')) {
      const from = req.query.rfNo.split('-')[0];
      const to = req.query.rfNo.split('-')[1];
      query.whereBetween('rfNo', [from, to]);
      Count.whereBetween('rfNo', [from, to]);
    } else {
      query.where('rfNo', req.query.rfNo);
      Count.where('rfNo', req.query.rfNo);
    }
  }
  if (req.query.fanModel && req.query.fanModel !== '') {
    query.where('fanModel', 'ilike', '%' + req.query.fanModel + '%');
    Count.where('fanModel', 'ilike', '%' + req.query.fanModel + '%');
  }

  if (req.query.fanType && req.query.fanType !== '') {
    query.where(
      db.raw('split_part("FanList"."fanModel",\' \',1) ilike ?', [
        '%' + req.query.fanType + '%'
      ])
    );
    Count.where(
      db.raw('split_part("FanList"."fanModel",\' \',1) ilike ?', [
        '%' + req.query.fanType + '%'
      ])
    );
  }

  if (req.query.customerName && req.query.customerName !== '') {
    query.where('customerName', 'ilike', '%' + req.query.customerName + '%');
    Count.where('customerName', 'ilike', '%' + req.query.customerName + '%');
  }
  if (req.query.application && req.query.application !== '') {
    query.where('application', 'ilike', '%' + req.query.application + '%');
    Count.where('application', 'ilike', '%' + req.query.application + '%');
  }
  if (req.query.projectName && req.query.projectName !== '') {
    query.where('projectName', 'ilike', '%' + req.query.projectName + '%');
    Count.where('projectName', 'ilike', '%' + req.query.projectName + '%');
  }
  if (req.query.flow && req.query.flow !== '') {
    if (req.query.flow.includes('-')) {
      const from = req.query.flow.split('-')[0];
      const to = req.query.flow.split('-')[1];
      query.whereBetween('flow', [from, to]);
      Count.whereBetween('flow', [from, to]);
    } else {
      query.where('flow', req.query.flow);
      Count.where('flow', req.query.flow);
    }
  }
  if (req.query.staticPressure && req.query.staticPressure !== '') {
    if (req.query.staticPressure.includes('-')) {
      const from = req.query.staticPressure.split('-')[0];
      const to = req.query.staticPressure.split('-')[1];
      query.whereBetween('staticPressure', [from, to]);
      Count.whereBetween('staticPressure', [from, to]);
    } else {
      query.where('staticPressure', req.query.staticPressure);
      Count.where('staticPressure', req.query.staticPressure);
    }
  }

  if (req.query.price && req.query.price !== '') {
    if (req.query.price.includes('-')) {
      const from = req.query.price.split('-')[0];
      const to = req.query.price.split('-')[1];
      query.whereBetween('price', [from, to]);
      Count.whereBetween('price', [from, to]);
    } else {
      query.where('price', req.query.price);
      Count.where('price', req.query.price);
    }
  }
  if (req.query.temperature && req.query.temperature !== '') {
    if (req.query.temperature.includes('-')) {
      const from = req.query.temperature.split('-')[0];
      const to = req.query.temperature.split('-')[1];
      query.whereBetween('temperature', [from, to]);
      Count.whereBetween('temperature', [from, to]);
    } else {
      query.where('temperature', req.query.temperature);
      Count.where('temperature', req.query.temperature);
    }
  }

  if (req.query.density && req.query.density !== '') {
    if (req.query.density.includes('-')) {
      const from = req.query.density.split('-')[0];
      const to = req.query.density.split('-')[1];
      query.whereBetween('density', [from, to]);
      Count.whereBetween('density', [from, to]);
    } else {
      query.where('density', req.query.density);
      Count.where('density', req.query.density);
    }
  }

  if (req.query.shaftPower && req.query.shaftPower !== '') {
    if (req.query.shaftPower.includes('-')) {
      const from = req.query.shaftPower.split('-')[0];
      const to = req.query.shaftPower.split('-')[1];
      query.whereBetween('shaftPower', [from, to]);
      Count.whereBetween('shaftPower', [from, to]);
    } else {
      query.where('shaftPower', req.query.shaftPower);
      Count.where('shaftPower', req.query.shaftPower);
    }
  }
  if (req.query.fanSpeed && req.query.fanSpeed !== '') {
    if (req.query.fanSpeed.includes('-')) {
      const from = req.query.fanSpeed.split('-')[0];
      const to = req.query.fanSpeed.split('-')[1];
      query.whereBetween('fanSpeed', [from, to]);
      Count.whereBetween('fanSpeed', [from, to]);
    } else {
      query.where('fanSpeed', req.query.fanSpeed);
      Count.where('fanSpeed', req.query.fanSpeed);
    }
  }
  if (req.query.motorPower && req.query.motorPower !== '') {
    if (req.query.motorPower.includes('-')) {
      const from = req.query.motorPower.split('-')[0];
      const to = req.query.motorPower.split('-')[1];
      query.whereBetween('motorPower', [from, to]);
      Count.whereBetween('motorPower', [from, to]);
    } else {
      query.where('motorPower', req.query.motorPower);
      Count.where('motorPower', req.query.motorPower);
    }
  }
  if (req.query.motorSpeed && req.query.motorSpeed !== '') {
    if (req.query.motorSpeed.includes('-')) {
      const from = req.query.motorSpeed.split('-')[0];
      const to = req.query.motorSpeed.split('-')[1];
      query.whereBetween('motorSpeed', [from, to]);
      Count.whereBetween('motorSpeed', [from, to]);
    } else {
      query.where('motorSpeed', req.query.motorSpeed);
      Count.where('motorSpeed', req.query.motorSpeed);
    }
  }
  if (req.query.yearOfMfg && req.query.yearOfMfg !== '') {
    if (req.query.yearOfMfg.includes('-')) {
      const from = req.query.yearOfMfg.split('-')[0];
      const to = req.query.yearOfMfg.split('-')[1];
      query.whereBetween('yearOfMfg', [from, to]);
      Count.whereBetween('yearOfMfg', [from, to]);
    } else {
      query.where('yearOfMfg', req.query.yearOfMfg);
      Count.where('yearOfMfg', req.query.yearOfMfg);
    }
  }

  if (
    req.query.pageSize &&
    req.query.pageSize !== '' &&
    req.query.index &&
    req.query.index !== ''
  ) {
    const pageSize = parseInt(req.query.pageSize);
    const index = parseInt(req.query.index);
    query.limit(pageSize).offset(index * pageSize);
  }
  if (Object.keys(req.query).filter(x => x.includes('Sort')).length === 0) {
    query.orderBy('rfNo', 'desc');
  }
  if (Object.keys(req.query).filter(x => x.includes('Sort')).length === 1) {
    const querySort = Object.keys(req.query).filter(x => x.includes('Sort'));
    const sortingField = querySort[0].split('Sort')[0];
    query.orderBy(`${sortingField}`, req.query[querySort[0]]);
  }

  const promData = Promise.all([query, Count])
    .then(results => {
      db.destroy();

      return res.json({
        data: results[0],
        count: parseInt(results[1][0].count)
      });
    })
    .catch(e => {
      return res.status(500).json(e);
    });
});

router.get('/files/count', (req, res) => {
  let deleteFile = false;
  Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
      if (!arr.includes(this[i])) {
        arr.push(this[i]);
      }
    }
    return arr;
  };
  fs.readdir('./Curves/', (err, files) => {
    if (!err) {
      const sortFiles = files
        .sort()
        .filter(x => getRangeCurve(x) !== undefined)
        .map(x => getRangeCurve(x));

      const rangeCurvedFiles = sortFiles
        .filter(
          x => /[a-z]/.test(x.start) === false && /[a-z]/.test(x.end) === false
        )
        .map(x => ({
          ...x,
          ...{
            start: x.start.trim(),
            end:
              x.start.trim().length > x.end.trim().length
                ? x.start
                    .trim()
                    .substring(0, x.start.trim().length - x.end.trim().length) +
                  x.end.trim()
                : x.end.trim()
          }
        }))
        .filter(
          x =>
            parseInt(req.query.rfNo) >= x.start &&
            parseInt(req.query.rfNo) <= x.end
        )
        .unique();

      fs.readdir('./GA Drawings/', (err, files) => {
        if (!err) {
          const sortFiles = files
            .sort()
            .filter(x => getGaDrawings(x) !== undefined)
            .map(x => getGaDrawings(x));

          const rangeGaDrawningFiles = sortFiles
            .filter(
              x =>
                parseInt(req.query.rfNo) >= x.start &&
                parseInt(req.query.rfNo) <= x.end
            )
            .unique();

          fs.readdir('./PO/', (err, files) => {
            if (!err) {
              const sortFiles = files
                .sort()
                .filter(x => getRangePo(x) !== undefined)
                .map(x => getRangePo(x));

              const rangePoFiles = sortFiles
                .filter(
                  x =>
                    /[a-z]/.test(x.start) === false &&
                    /[a-z]/.test(x.end) === false
                )
                .map(x => ({
                  ...x,
                  ...{
                    start: x.start.trim(),
                    end:
                      x.start.trim().length > x.end.trim().length
                        ? x.start
                            .trim()
                            .substring(
                              0,
                              x.start.trim().length - x.end.trim().length
                            ) + x.end.trim()
                        : x.end.trim()
                  }
                }))
                .filter(
                  x =>
                    parseInt(req.query.rfNo) >= x.start &&
                    parseInt(req.query.rfNo) <= x.end
                )
                .unique();

              return res.status(200).json([
                {
                  CurveFiles: rangeCurvedFiles.length,
                  GaDrawningFiles: rangeGaDrawningFiles.length,
                  PoFiles: rangePoFiles.length
                }
              ]);
            }
          });
        }
      });
    }
  });
});
module.exports = router;
