var express = require('express');
const router = express.Router();
const auth = require('../authentication/auth')();
const knex = require('knex');
const dbConfig = require('../knexfile').development;
const xlsxtojson = require('xlsx-to-json-lc');
const xlstojson = require('xls-to-json-lc');
const multer = require('multer');
const fs = require('fs');
const path=require('path');
var t = require('tcomb-validation');
var validations =  require('./validations');
var PdfReader = require('pdfreader').PdfReader;
var PDFParser=require('pdf2json');
var pdfUtil = require('pdf-to-text');
var imageToTextDecoder = require('image-to-text');
var pdf_extract = require('pdf-extract');
var inspect = require('eyes').inspector({maxLength:20000});
const PDFMerge = require('pdf-merge');
var merge = require('easy-pdf-merge');
var base64Img = require('base64-img');


router.get('/list', (req, res) => {
   
    

    fs.readdir('./Customer Logos', (err, files) => {
        if (!err) {
        
     return res.status(200).json(files)
        }
        else {
            return res.status(500).json(err) 
        }
      })
  });

router.get('/convertbase64', (req, res) => {
 
    fs.readdir('./Customer Logos', (err, files) => {
        if (!err) {

        if(req.query.logoname){
            
            var filepath = path.join('./Customer Logos', req.query.logoname);
            
            base64Img.base64(filepath , function (err,data){
                if(!err){
                  
                   return res.status(200).json(data);
                }
                else {
                    return res.status(500).json(err)
                } 
            });
           
          }
          else{
            return res.status(400).json({message:'image not found'})
          }
        }
        else{
            return res.status(500).json(err)
        }

      })
  });

  router.get('/staticpages', (req, res) => {
 
    fs.readdir('./StaticPages', (err, files) => {
        if (!err) {

        if(req.query.imagename){
           
            var filepath = path.join('./StaticPages', req.query.imagename);
            
            base64Img.base64(filepath , function (err,data){
                if(!err){
                  
                   return res.status(200).json(data);
                }
                else {
                    return res.status(500).json(err)
                } 
            });
           
          }
          else{
            return res.status(400).json({message:'image not found'})
          }
        }
        else{
            return res.status(500).json(err)
        }

      })
  });
  module.exports = router; 
  