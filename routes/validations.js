const t = require('tcomb');

var data=t.struct({
    rfNo:t.String,
    customerName:t.String,
    projectName:t.String,
    application:t.String,
    fanModel:t.String,
    flow:t.Number,
    staticPressure:t.Number,
    temperature:t.Number,
    density:t.Number,
    shaftPower:t.Number,
    fanSpeed:t.Number,
    motorPower:t.Number,
    motorSpeed:t.Number,
    yearOfMfg:t.Number,
    price:t.String,
    isGaDrawing:t.Boolean,
    isFanCurves:t.Boolean,
    isActive:t.Boolean
    });



 const StringNumber = t.refinement(t.String, s => !isNaN(s))

 var pagination=t.struct({
         pageSize:StringNumber,
         index:StringNumber
     })


var input=t.struct({
    id:StringNumber
})
    
const downloadValid=t.struct({
    type:t.enums.of(['poDownload','gadrawningDownload','curvesDownload'])
})

const validateRegisterRequest = t.struct({
    userName: t.String,
    emailId:t.String,
    password: t.String,
    phoneNumber: t.String,
    role: t.String, 
  });
  
  const login = t.struct({
    userName: t.String,
    password: t.String
  });

module.exports ={data,pagination,input,downloadValid,validateRegisterRequest,login};
