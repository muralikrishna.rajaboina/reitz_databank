var express = require('express');
const router = express.Router();
const knex = require('knex');
const dbConfig = require('../knexfile').development;
var t = require('tcomb-validation');
var validations =  require('./validations');
const auth = require('../authentication/auth')();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const LocalStrategy = require('passport-local').Strategy;
const cfg = require('../authentication/jwt_config.js');
const SendOtp = require('sendotp');
const sendOtp = new SendOtp('AuthKey');
const https = require('https');
var nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const { URL } = require('url');

/* GET home page. */
const fs = require('fs');
const reqRegisterValidation = (req, res, next) => {
  let result = t.validate(req.body, validations.validateRegisterRequest);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};

router.post('/register',reqRegisterValidation, async(req, res) => {
  console.log(req);
  let db;
  try{
    db = knex(dbConfig);

  const hashedPassword = bcrypt.hashSync(req.body.password, 10);
    const user= await db.returning('*')
     .insert({
     userName:req.body.userName,
     emailId:req.body.emailId,
     password:hashedPassword,
     phoneNumber:req.body.phoneNumber,
     role:req.body.role,
     isActive:false
   })
   .into('Users')
   
    await db.destroy();
  if(user)
   { 
     res.status(200).json({ data:user[0],message: ' User added sucessfully' });
   }
   else{
     return res.status(500).json({ message: 'User not added' });
   }
  }
  catch(error){
    await db.destroy();
    return res.status(500).json({ message: 'something wrong' });
  }
 })
  
const reqloginValidation = (req, res, next) => {
  let result = t.validate(req.body, validations.login);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};


router.post('/signIn', reqloginValidation, async (req, res) => {
  let db;
  try {
    db = knex(dbConfig);
  
  const x = await db
      .select(db.raw(`UPPER(REPLACE("userName", ' ', ''))`),'phoneNumber','role','password','isActive')
     .from('Users')
     .where(db.raw(`UPPER(REPLACE("userName", ' ', ''))`),req.body.userName.replace(/ /g,'').toUpperCase())
     .andWhere('isActive',true)
   
  if (bcrypt.compareSync(req.body.password, x[0].password)) {
    
    res.status(200).json({
      token: jwt.sign({ userName: req.body.userName }, cfg.jwtSecret),
      user: {...x[0], roleId:x[0].role === 'employee'?1:x[0].role === 'admin'?2:x[0].role === 'superAdmin'?3 : 0}
    });
  } else {
    res.status(404).json('User not Existed');
  }  
  } catch (error) {
     return res.status(500).json({
       message: error.message
     }) 
  }
});

router.post('/generateotp', async(req, res) => {
  const db = knex(dbConfig);
       try{
  const deleteotp=await db.where('phoneNumber', req.body.phoneNumber).del().from('otpData')
  const x = await db.select('userName','phoneNumber','isActive')
   .from('Users')
   .where({
         phoneNumber: req.body.phoneNumber
      })
      // checks whether phone number exists or not
      if(x[0]){
      //checks user status is in Active or not
       if(x[0].isActive===false){
          function generateRandomNumber() 
              {
                  return Math.random() * (999999-100000) + 100000 ;
               }
               const random= Math.round(generateRandomNumber())
               console.log(random)
              
        https.get(`https://api.mVaayoo.com/mvaayooapi/MessageCompose?user=sridhar.polus@gmail.com:Design_20&senderID=TEST%20SMS&receipientno=${req.body.phoneNumber}&dcs=0&msgtxt=Your%20OTP%20is%20${random}&state=4`, (resp) => {
       
        });
        const otpData=await db.insert({
           
          phoneNumber:req.body.phoneNumber,
          otp:random,
          isActive:true
        })
        .into('otpData')
     
        return res.status(200).json({message:'One Time Password Generated Successfully'}) 
      }
      else{
        return res.status(200).json({message:'User already registered'})
      }
      }
      else{
        return res.status(400).json({message:'user not existed'})
      } 
    }
    catch(error){
      return res.status(500).json({message:'something wrong'})
    } 

 })

 router.post('/authenticate', async(req, res) => {
  const db = knex(dbConfig);
  function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
const newPassword=randomPassword(10)
const hashedPassword = bcrypt.hashSync(newPassword, 10);

  const x = await db.select('otp','phoneNumber')
          .from('otpData')
         .where({
         otp: req.body.otp,
         phoneNumber: req.body.phoneNumber
         })
     
      if(x[0]){
        // send  OTP and verify OTP if it is suceess then redirecting to login page.
        
         const deleteotp=await db.where('phoneNumber', req.body.phoneNumber).del().from('otpData')
     
     const changeStatus=await db.from('Users').select('*')
     .update({password:hashedPassword,isActive:true})
     .where({phoneNumber:req.body.phoneNumber})
     .returning('*')

     var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'muralikrishna.rajaboina@technoidentity.com',
        pass: 'Design_20'
      }
    });
   
    var mailOptions = {
      from: 'muralikrishna.rajaboina@technoidentity.com',
      to:`${changeStatus[0].emailId}` ,
      subject: 'Reitz Data Bank Credentials',
      text: ` Welcome Greetings ${changeStatus[0].userName},
  
                UserName: ${changeStatus[0].userName}
                Password: ${newPassword}`
    };
    
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log('some error')
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
        return res.status(200).json({message:'authentication success'}) 
      }
      else{
      
        return res.status(400).json({message:'authentication failed'})
      }  
 })
 
 router.get('/readfile', (req, res) => {
  const fileUrl = new URL('http://www.africau.edu/images/default/sample.pdf');
  
  fs.readFileSync(fileUrl);
 })
module.exports = router;
